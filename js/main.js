var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Aplicando el modelo de gestión organizacional basado en resultados",
    autor: "Edilson Laverde Molina",
    date: "01/03/2021",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios=[
    {url:"sonidos/click.mp3",     name:"click" },
    {url:"sonidos/fail.mp3",      name:"fail" },
    {url:"sonidos/good.mp3",      name:"good" },
    {url:"sonidos/engranaje.mp3", name:"engranaje" },
    {url:"sonidos/intro.mp3",     name:"intro" },
    {url:"sonidos/Audio_1.mp3"   ,name:"audio1" },
    {url:"sonidos/Audio_2.mp3"   ,name:"audio2" }
];
var contador1=0;
var good1=0;
var contador2=0;
var good2=0;
var contador3=0;
var good3=0;
var p1=0;
var p2=0;
var p3=0;

var test1=[
    {
        text:"Aportar la experiencia y conocimientos adquiridos al desarrollo de la labor.",
        grupo:"1"
    },
    {
        text:"Mantener la motivación sobre el cumplimiento de las metas propuestas.",
        grupo:"1"
    },
    {
        text:"Innovar frente a la ejecución de cada actividad.",
        grupo:"1"
    },
    {
        text:"Aportar a la productividad organizacional y, por ende, al crecimiento personal.",
        grupo:"1"
    },
    {
        text:"Consensuar con la empresa los horarios laborales para acceder a la formación profesional.",
        grupo:"1"
    },
    {
        text:"Definir acciones que contribuyan a la responsabilidad social empresarial.",
        grupo:"1"
    },
    {
        text:"Apoyar a los colaboradores con el cuidado y/o formación de sus menores o adolescentes.",
        grupo:"1"
    },
    {
        text:"Programar actividades relámpago de integración en las pausas activas.",
        grupo:"1"
    },
    {
        text:"Mantener buenas relaciones con los compañeros de trabajo.",
        grupo:"2"
    },
    {
        text:"Verificar que no haya objetos distintos a los requeridos para la actividad a realizar.",
        grupo:"2"
    },
    {
        text:"Contribuir al trabajo en equipo.",
        grupo:"2"
    },
    {
        text:"Mantener aseado el lugar de trabajo.",
        grupo:"2"
    },
    {
        text:"Verificar que la iluminación y la ventilación sean apropiadas para el desarrollo de la actividad.",
        grupo:"2"
    },
    {
        text:"Confirmar que el puesto de trabajo se adecúa a las necesidades físicas.",
        grupo:"2"
    },
    {
        text:"Comprobar que las herramientas disponibles para el desarrollo de la actividad son las idóneas y están en buen estado.",
        grupo:"2"
    },
    {
        text:"Realizar un balance, al terminar cada jornada, sobre la labor realizada, tanto individual como colectiva.",
        grupo:"2"
    },
    {
        text:"Documentar cada proceso o acción nueva realizada en el puesto de trabajo.",
        grupo:"3"
    },
    {
        text:"Alistar los insumos necesarios antes de iniciar la actividad.",
        grupo:"3"
    },
    {
        text:"Realizar las actividades o los procesos con calidad desde su inicio.",
        grupo:"3"
    },
    {
        text:"Cumplir con las labores asignadas en el tiempo estipulado.",
        grupo:"3"
    },
    {
        text:"Reducir el desperdicio de las materias primas a su mínima expresión.",
        grupo:"3"
    },
    {
        text:"Definir indicadores de eficiencia y eficacia por cada proceso realizado.",
        grupo:"3"
    },
    {
        text:"Programar las líneas de producción de acuerdo a los requerimientos del punto de venta.",
        grupo:"3"
    },
    {
        text:"Proyectar y planear el mantenimiento de la maquinaria utilizada en el proceso productivo.",
        grupo:"3"
    }
];
var test2=[
    {
        text:"Colabora a sus compañeros cuando requieren apoyo.",
        grupo:"1"
    },
    {
        text:"Verifica que el lugar de trabajo siempre tenga buena ventilación e iluminación.",
        grupo:"1"
    },
    {
        text:"Mantiene una comunicación asertiva con los demás compañeros de trabajo.",
        grupo:"1"
    },
    {
        text:"Las herramientas o maquinaria de que dispone para la labor están en buen estado de funcionamiento.",
        grupo:"1"
    },
    {
        text:"En caso de alguna avería en las herramientas disponibles, reporta inmediatamente.",
        grupo:"1"
    },
    {
        text:"Siempre realiza sus actividades eficientemente.",
        grupo:"1"
    },
    {
        text:"Mantiene el lugar de trabajo en buen estado de aseo.",
        grupo:"1"
    },
    {
        text:"Los tiempos de descanso y ocio están bien diseñados y son acordes frente a la actividad realizada.",
        grupo:"2"
    },
    {
        text:"El producto / servicio terminado cumple con los estándares establecidos.",
        grupo:"2"
    },
    {
        text:"El diseño ergonómico del lugar de trabajo se adapta a las necesidades particulares de cada colaborador.",
        grupo:"2"
    },
    {
        text:"En general, el estilo de liderazgo de la empresa es motivador e incita a trabajar cada vez mejor.",
        grupo:"2"
    },
    {
        text:"El lugar de trabajo tiene todas las condiciones de espacio necesarias para desarrollar la actividad.",
        grupo:"2"
    },
    {
        text:"Se aplican los indicadores de productividad y eficiencia al finalizar la jornada.",
        grupo:"2"
    },
    {
        text:"La modalidad de contrato y la remuneración son coherentes frente a la actividad realizada.",
        grupo:"2"
    },
    {
        text:"Verifica que el producto / servicio terminado cumpla con las políticas de calidad de la organización.",
        grupo:"3"
    },
    {
        text:"Aporta conocimiento y experiencia a sus compañeros de trabajo a fin de coadyuvar al logro de los resultados propuestos.",
        grupo:"3"
    },
    {
        text:"Propone resultados colectivos por periodos de tiempo (diarios, semanales) y las estrategias para lograrlos.",
        grupo:"3"
    },
    {
        text:"Aplica los conocimientos y habilidades adquiridas en la labor realizada.",
        grupo:"3"
    },
    {
        text:"Ejerce autosupervisión y autocontrol en las actividades realizadas y en sus resultados finales.",
        grupo:"3"
    },
    {
        text:"Se empodera para resolver cualquier situación imprevista que ocasione inconvenientes.",
        grupo:"3"
    },
    {
        text:"Establece metas diarias de producción y define acciones para lograrlas.",
        grupo:"3"
    },

]

var test3=[
    {
        text:"La filosofía es hacer el trabajo bien desde el principio.",
        grupo:"1"
    },
    {
        text:"Se realiza una supervisión, control, seguimiento, evaluación y realimentación a las labores realizadas periódicamente.",
        grupo:"1"
    },
    {
        text:"Los clientes solicitan sus pedidos y sobre esto se programa la producción y entrega del producto.",
        grupo:"1"
    },
    {
        text:"Los puestos de trabajo están diseñados para que la labor se realice cómoda y agradablemente.",
        grupo:"1"
    },
    {
        text:"Se ha programado realizar un control a la calidad del proceso productivo en partes específicas del mismo.",
        grupo:"1"
    },
    {
        text:"El estilo de liderazgo es participativo, lo que propicia un buen clima organizacional apto para la productividad.",
        grupo:"1"
    },
    {
        text:"Satisfacción del cliente con el producto terminado y de buena calidad.",
        grupo:"1"
    },
    {
        text:"Copiamos los diseños para estar de acuerdo con las tendencias de la moda.",
        grupo:"2"
    },
    {
        text:"Solicitamos a los clientes uno o dos días de plazo para entregar sus pedidos.",
        grupo:"2"
    },
    {
        text:"Cada colaborador es responsable de su propia labor asignada.",
        grupo:"2"
    },
    {
        text:"Se hace una inspección final al producto, una vez terminado, antes de ser entregado.",
        grupo:"2"
    },
    {
        text:"Es necesario rotar las herramientas disponibles para poder realizar la producción.",
        grupo:"2"
    },
    {
        text:"La producción se realiza para tener stock de inventario.",
        grupo:"2"
    },
]
function main(sym) {
var items=[];
var udec = ivo.structure({
        created:function(){
           var t=this;
           test1 = test1.sort(function() {return Math.random() - 0.5});
           test2 = test2.sort(function() {return Math.random() - 0.5});
           test3 = test3.sort(function() {return Math.random() - 0.5});
            //precarga audios//
           ivo.load_audio(audios,onComplete=function(){
               t.push_arratrables();
               t.animation();
               t.events();
               ivo(ST+"preload").hide();
           });
        },
        methods: {
            push_arratrables:function(){
                for([index,t1] of test1.entries()){
                    ivo(ST+"box1").append(`<div id="grupo1_${index}" class="arrastrable" data-origin="g${t1.grupo}">${t1.text}</div>`);
                }
                for([index,t1] of test2.entries()){
                    ivo(ST+"box2").append(`<div id="grupo2_${index}" class="arrastrable" data-origin="g${t1.grupo}">${t1.text}</div>`);
                }
                for([index,t1] of test3.entries()){
                    ivo(ST+"box3").append(`<div id="grupo3_${index}" class="arrastrable" data-origin="g${t1.grupo}">${t1.text}</div>`);
                }

                ivo(ST+"t1").html(`
                <div class="page">
                    <h1>Objetivo</h1>
                    <hr>
                    <p>Aplicar el modelo de gestión organizacional basado en resultados a un caso particular, identificando objetivos, acciones y premisas asociadas a las distintas dimensiones del modelo propuesto.</p>
                </div>
                `)
                ivo(ST+"t2").html(`
                <div class="page">
                    <h1>Contexto</h1>
                    <hr>
                    <p>Tomando como referencia los conceptos analizados respecto a la administración por objetivos –APO– y la administración por resultados –APR–, se analizarán, a partir de su aplicación en un caso particular, cada una de las dimensiones del ser humano y, sus variables, definidas en el modelo de gestión organizacional basado en resultados.</p>
                </div>
                `)
                ivo(ST+"t3").html(`
                <div class="page">
                    <h1>Instrucción</h1>
                    <hr>
                    <p>A través de esta actividad usted aplicará el modelo de gestión organizacional basado en resultados a partir de un caso particular. Para conseguirlo deberá identificar los componentes planteados en cada uno de los tres (3) niveles de aplicación del modelo: 1) objetivos, 2) acciones, y 3) premisas. Cada componente identificado representará un nivel superado. De esta manera, la sumatoria de los componentes identificados significará la aplicación total o parcial del modelo de gestión organizacional basado en resultados respecto al caso planteado.</p>
                    <p>Cada componente identificado, y el correspondiente nivel superado, equivale a un máximo de tres (3) puntos, por lo cual, el puntaje máximo a obtener es de nueve (9) puntos. Cuenta con sólo una (1) oportunidad para la realización de esta actividad. ¡Adelante!</p>
                </div>
                `)
            },
            events:function(){
                var t=this;
                ivo(".textos").hide();
                ivo(ST+"btn_iniciar").on("click",function(){
                    stage1.reverse().timeScale(3);
                    stage2.play().timeScale(1);
                    ivo.play("audio1");
                });
                ivo(ST+"btn_sig_1").on("click",function(){
                    stage2.reverse().timeScale(3);
                    stage3.play().timeScale(1);
                });
                ivo(ST+"btn_sig_2").on("click",function(){
                    stage3.reverse().timeScale(3);
                    stage4.play().timeScale(1);
                });
                ivo(ST+"creditos").on("click",function(){
                    creditos.reverse().timeScale(3);
                });
                ivo(ST+"btn_credito").on("click",function(){
                    creditos.play().timeScale(1);
                });
                ivo(ST+"cerrar1").on("click",function(){
                    ivo(ST+"objetivo").hide();
                });
                ivo(ST+"cerrar2").on("click",function(){
                    ivo(ST+"contexto").hide();
                });
                ivo(ST+"cerrar3").on("click",function(){
                    ivo(ST+"instruccion").hide();
                });
                ivo(ST+"btn_caso").on("click",function(){
                    window.open("pdf/Caso.pdf","_blank");
                });

                ivo(ST+"btn_instruccion").on("click",function(){
                    ivo(ST+"instruccion").show();
                });
                ivo(ST+"btn_objetivo").on("click",function(){
                    ivo(ST+"objetivo").show();
                });
                ivo(ST+"btn_contexto").on("click",function(){
                    ivo(ST+"contexto").show();
                });
                ivo(ST+"btn_info").on("click",function(){
                    ivo(ST+"tf").html(`
                    <p>1. A continuación encontrará las <b>dimensiones</b> del ser humano establecidas en el modelo de gestión organizacional basado en resultados con el fin de que aplique dicho modelo según el caso presentado. Como refuerzo de la actividad realizada en el REA 1 del curso, identifique dentro de las opciones que aparecen a la izquierda los <b>objetivos asociados a cada dimensión</b> y arrástrelos a la columna correspondiente.</p>

                    2. Puntos a obtener:<br>
                    De 0 a 8 aciertos: 1 punto<br>
                    De 9 a 16 aciertos: 2 puntos<br>
                    De 17 a 24 ciertos: 3 puntos<br>
                    `);
                    final.play();
                });
                ivo(ST+"btn_info2").on("click",function(){
                    ivo(ST+"tf").html(`
                    <p>1. A continuación encontrará un esquema de las <b>interacciones entre las dimensiones</b> del ser humano establecidas en el modelo de gestión organizacional basado en resultados. Como refuerzo de la actividad realizada en el REA 2 del curso, identifique dentro de las opciones que aparecen a la izquierda, según el caso presentado, las <b>acciones asociadas a cada interacción</b> y arrástrelas a la columna correspondiente.</p>

                    2. Puntos a obtener:<br>
                    De 0 a 7 aciertos: 1 punto<br>
                    De 8 a 14 aciertos: 2 puntos<br>
                    De 15 a 21 ciertos: 3 puntos<br>
                    `);
                    final.play();
                });
                ivo(ST+"btn_info3").on("click",function(){
                    final.play();
                    ivo(ST+"tf").html(`
                    <p>1. A continuación encontrará un esquema del <b>objetivo cumplido o resultados esperados,</b> de acuerdo a como convergen las tres intersecciones de las dimensiones del ser humano establecidas en el modelo de gestión organizacional basado en resultados. Identifique en esta fase final de la actividad, dentro de las opciones que aparecen a la izquierda, según el caso presentado, las <b>siete (7) premisas asociadas coherentemente con el resultado</b> de las interacciones entre las dimensiones del modelo de gestión propuesto y arrástrelas a la columna correspondiente.</p>

                    2. Puntos a obtener:<br>
                    De 0 a 2 aciertos: 1 punto<br>
                    De 3 a 5 aciertos: 2 puntos<br>
                    De 6 a 7 ciertos: 3 puntos<br>
                    `);
                });
                ivo(ST+"instruccion_final").on("click",function(){
                    final.reverse();
                });

                $( ".arrastrable" ).draggable({revert: true});
                $( ".contenedores" ).droppable({
                    drop: function( event, ui ) {
                        if($(this).attr("data-origin")!="none"){
                            if($(this).hasClass($(ui.draggable).attr("data-origin"))){
                                ivo.play("good");
                                $(this).css("background-color","#D7EBBC");
                                good1+=1;
                            }else{
                                ivo.play("fail");
                                $(this).css("background-color","#F9B0B3");
                            }
                            contador1+=1;
                            $(this).attr("data-origin","none");
                            $(this).text($(ui.draggable).text());
                            $(ui.draggable).hide();
                            if(contador1==24){
                                let legend="";
                                if(good1<=8){
                                    legend="1 Punto";
                                    p1=1;
                                }
                                if(good1>=9 && good1<=16){
                                    legend="2 Puntos";
                                    p1=2;
                                }
                                if(good1>16){
                                    legend="3 Puntos";
                                    p1=3;
                                }
                                swal({
                                    position: 'top-end',
                                    title: legend,
                                    showConfirmButton: false,
                                    timer: 15500
                                });
                                stage4.reverse().timeScale(3);
                                stage5.play().timeScale(1);
                            }
                        }
                    },
                    over: function(event, ui){
                        if($(this).attr("data-origin")!="none"){
                            $( this ).css("background","#E9B02D");
                        }
                    },
                    out:function(event, ui){
                        if($(this).attr("data-origin")!="none"){
                            $( this ).css("background","rgba(255, 255, 255, 0.03)");
                        }
                    }
                });
                $( ".contenedores2" ).droppable({
                    drop: function( event, ui ) {
                        if($(this).attr("data-origin")!="none"){
                            if($(this).hasClass($(ui.draggable).attr("data-origin"))){
                                ivo.play("good");
                                $(this).css("background-color","#D7EBBC");
                                good2+=1;
                            }else{
                                ivo.play("fail");
                                $(this).css("background-color","#F9B0B3");
                            }
                            contador2+=1;
                            $(this).attr("data-origin","none");
                            $(this).text($(ui.draggable).text());
                            $(ui.draggable).hide();
                            if(contador2==21){
                                let legend="";
                                if(good2<=7){
                                    legend="1 Punto";
                                    p2=1;
                                }
                                if(good2>=8 && good2<=14){
                                    legend="2 Puntos";
                                    p2=2;
                                }
                                if(good2>14){
                                    legend="3 Puntos";
                                    p2=3;
                                }
                                swal({
                                    position: 'top-end',
                                    title: legend,
                                    showConfirmButton: false,
                                    timer: 15500
                                });
                                stage5.reverse().timeScale(3);
                                stage6.play().timeScale(1);
                            }
                        }
                    },
                    over: function(event, ui){
                        if($(this).attr("data-origin")!="none"){
                            $( this ).css("background","#E9B02D");
                        }
                    },
                    out:function(event, ui){
                        if($(this).attr("data-origin")!="none"){
                            $( this ).css("background","rgba(255, 255, 255, 0.03)");
                        }
                    }
                });
                $( ".contenedores3" ).droppable({
                    drop: function( event, ui ) {
                        if($(this).attr("data-origin")!="none"){
                            if($(this).hasClass($(ui.draggable).attr("data-origin"))){
                                ivo.play("good");
                                $(this).css("background-color","#D7EBBC");
                                good3+=1;
                            }else{
                                ivo.play("fail");
                                $(this).css("background-color","#F9B0B3");
                            }
                            contador3+=1;
                            $(this).attr("data-origin","none");
                            $(this).text($(ui.draggable).text());
                            $(ui.draggable).hide();
                            if(contador3==7){
                                let legend="";
                                if(good3<=2){
                                    legend="1 Punto";
                                    p3=1;
                                }
                                if(good3>=3 && good3<=5){
                                    legend="2 Puntos";
                                    p3=2;
                                }
                                if(good3>=6){
                                    legend="3 Puntos";
                                    p3=3;
                                }
                                swal({
                                    position: 'top-end',
                                    title: legend,
                                    showConfirmButton: false,
                                    timer: 15500
                                });
                                stage6.reverse().timeScale(3);
                                retroalimentacion.play();
                                p=p1+p2+p3;
                                console.log(p);
                                if(p<=5){
                                    $(ST+"Text4").html(`Ánimo, siga intentándolo, recuerde la importancia de revisar los recursos educativos del curso para fortalecer su aprendizaje en torno al modelo propuesto de gestión organizacional basado en resultados. <center><h1>Nota equivalente: 30</h1></center>`);
                                    Scorm_mx = new MX_SCORM(false);
                                    console.log("Nombre: " + Scorm_mx.info_user().name + " Id: " + Scorm_mx.info_user().id+" Nota: "+p);
                                    Scorm_mx.set_score(30);
                                }
                                if(p>=6 && p<=8 ){
                                    $(ST+"Text4").html(`Va por buen camino, pero es necesario reforzar la lectura de los recursos educativos del curso para afianzar su aprendizaje en torno al modelo propuesto de gestión organizacional basado en resultados. <center><h1>Nota equivalente: 40</h1></center>`);
                                    Scorm_mx = new MX_SCORM(false);
                                    console.log("Nombre: " + Scorm_mx.info_user().name + " Id: " + Scorm_mx.info_user().id+" Nota: "+p);
                                    Scorm_mx.set_score(40);
                                }
                                if(p==9){
                                    $(ST+"Text4").html(`Muy bien, ha demostrado sus capacidades a la hora de aplicar el modelo propuesto de gestión organizacional basado en resultados. <center><h1>Nota equivalente: 50</h1></center>`);
                                    Scorm_mx.set_score(50);
                                }
                            }
                        }
                    },
                    over: function(event, ui){
                        if($(this).attr("data-origin")!="none"){
                            $( this ).css("background","#E9B02D");
                        }
                    },
                    out:function(event, ui){
                        if($(this).attr("data-origin")!="none"){
                            $( this ).css("background","rgba(255, 255, 255, 0.03)");
                        }
                    }
                });
            },
            animation:function(){

                stage0 = new TimelineMax();
                stage0.append(TweenMax.from(ST+"titulo", .8,          {x:1300,opacity:0}), 0);
                stage0.append(TweenMax.from(ST+"btn_credito", .8,          {x:1300,opacity:0}), 0);
                stage0.stop();

                stage1 = new TimelineMax({onStart:function(){stage0.play()}});
                stage1.append(TweenMax.from(ST+"stage1", .8,          {x:1300,opacity:0}), 0);
                stage1.append(TweenMax.staggerFrom(".obj1",.4,        {x:1500,opacity:0,y:-800,ease:Elastic.easeOut.config(0.3, 0.4)},.2), 0);
                stage1.append(TweenMax.from(ST+"btn_iniciar", .8,     {x:-1300,opacity:0}), 0);
                //stage1.stop();

                stage2 = new TimelineMax();
                stage2.append(TweenMax.from(ST+"stage2", .8,          {x:1300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"Recurso_15Copy2", .8, {x:-1300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"btn_sig_1", .8,       {x:1300,opacity:0}), 0);
                stage2.stop();

                stage3 = new TimelineMax();
                stage3.append(TweenMax.from(ST+"stage3", .8,          {x:-1300,opacity:0}), 0);
                stage3.append(TweenMax.from(ST+"inst", .8,            {x:1300,opacity:0}), 0);
                stage3.append(TweenMax.from(ST+"box", .8,             {x:-300,opacity:0}), 0);
                stage3.append(TweenMax.from(ST+"btn_caso", .8,        {x:-300,opacity:0}), 0);
                stage3.append(TweenMax.from(ST+"btn_sig_2", .8,       {y:-300,opacity:0}), 0);
                stage3.stop();

                stage4 = new TimelineMax();
                stage4.append(TweenMax.from(ST+"stage4", .8,          {x:1300,opacity:0}), 0);
                stage4.append(TweenMax.staggerFrom(".efect1",.4,      {x:1500,opacity:0,y:-800,ease:Elastic.easeOut.config(0.3, 0.4)},.2), 0);
                stage4.append(TweenMax.from(ST+"btn_caso", .8,        {x:-300,opacity:0}), 0);
                stage4.append(TweenMax.from(ST+"btn_info", .8,        {scale:0,rotation:9000,opacity:0}), 0);
                stage4.stop();

                stage5 = new TimelineMax();
                stage5.append(TweenMax.from(ST+"stage5", .8,          {x:1300,opacity:0}), 0);
                stage5.append(TweenMax.staggerFrom(".efect2",.4,      {x:1500,opacity:0,y:-800,ease:Elastic.easeOut.config(0.3, 0.4)},.2), 0);
                stage5.append(TweenMax.from(ST+"btn_info2", .8,       {scale:0,rotation:9000,opacity:0}), 0);
                stage5.stop();

                stage6 = new TimelineMax();
                stage6.append(TweenMax.from(ST+"stage6", .8,          {x:1300,opacity:0}), 0);
                stage6.append(TweenMax.staggerFrom(".efect3",.4,      {x:1500,opacity:0,y:-800,ease:Elastic.easeOut.config(0.3, 0.4)},.2), 0);
                stage6.append(TweenMax.from(ST+"btn_info3", .8,       {scale:0,rotation:9000,opacity:0}), 0);
                stage6.stop();

                creditos = new TimelineMax();
                creditos.append(TweenMax.from(ST+"creditos", .8,          {x:1300,opacity:0}), 0);
                creditos.stop();

                retroalimentacion = new TimelineMax({onStart:function(){
                    ivo.play("audio2");
                }});
                retroalimentacion.append(TweenMax.from(ST+"retroalimentacion", .8, {x:-4300,opacity:0}), 0);
                retroalimentacion.append(TweenMax.from(ST+"Group3", .8,            {x:1300,opacity:0}), 0);
                retroalimentacion.stop();

                final = new TimelineMax();
                final.append(TweenMax.from(ST+"instruccion_final", .8, {x:-4300,opacity:0}), 0);
                final.append(TweenMax.from(ST+"Group7", .8,            {x:1300,opacity:0}), 0);
                final.stop();
            }
        }
 });
}
