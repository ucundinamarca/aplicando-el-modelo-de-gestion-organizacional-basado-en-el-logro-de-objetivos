/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'fondo',
                            type: 'image',
                            rect: ['0px', '0px', '1025px', '638px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Recurso_1.png",'0px','0px']
                        },
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['0', '0', '1025', '638', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_2',
                                type: 'image',
                                rect: ['33px', '107px', '959px', '93px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_2.png",'0px','0px'],
                                userClass: "obj1"
                            },
                            {
                                id: 'Recurso_3',
                                type: 'image',
                                rect: ['211px', '189px', '652px', '2px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_3.png",'0px','0px'],
                                userClass: "obj1"
                            },
                            {
                                id: 'Recurso_4',
                                type: 'image',
                                rect: ['91px', '199px', '892px', '227px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_4.png",'0px','0px'],
                                userClass: "obj1"
                            },
                            {
                                id: 'btn_iniciar',
                                type: 'image',
                                rect: ['470px', '382px', '138px', '44px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_5.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage2',
                            type: 'group',
                            rect: ['28px', '8px', '813', '508', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_7',
                                type: 'image',
                                rect: ['226px', '115px', '650px', '393px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_7.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_8',
                                type: 'image',
                                rect: ['240px', '136px', '603px', '62px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_8.png",'0px','0px']
                            },
                            {
                                id: 'btn_sig_1',
                                type: 'image',
                                rect: ['719px', '524px', '138px', '44px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_10.png",'0px','0px']
                            },
                            {
                                id: 'cir',
                                type: 'image',
                                rect: ['413px', '206px', '287px', '267px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"cir.png",'0px','0px']
                            },
                            {
                                id: 'btn_contexto',
                                type: 'rect',
                                rect: ['580px', '359px', '132px', '70px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'btn_objetivo',
                                type: 'rect',
                                rect: ['491px', '228px', '132px', '70px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'btn_instruccion',
                                type: 'rect',
                                rect: ['412px', '359px', '132px', '70px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'objetivo',
                                type: 'rect',
                                rect: ['246px', '131px', '610px', '357px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgb(0, 0, 0)","none"],
                                userClass: "textos",
                                c: [
                                {
                                    id: 't1',
                                    type: 'text',
                                    rect: ['21px', '24px', '562px', '275px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                },
                                {
                                    id: 'cerrar1',
                                    type: 'image',
                                    rect: ['266px', '330px', '96px', '30px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"cerrar.png",'0px','0px']
                                }]
                            },
                            {
                                id: 'contexto',
                                type: 'rect',
                                rect: ['246px', '131px', '610px', '357px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgb(0, 0, 0)","none"],
                                userClass: "textos",
                                c: [
                                {
                                    id: 't2',
                                    type: 'text',
                                    rect: ['21px', '24px', '562px', '275px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                },
                                {
                                    id: 'cerrar2',
                                    type: 'image',
                                    rect: ['266px', '330px', '96px', '30px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"cerrar.png",'0px','0px']
                                }]
                            },
                            {
                                id: 'instruccion',
                                type: 'rect',
                                rect: ['246px', '131px', '610px', '357px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgb(0, 0, 0)","none"],
                                userClass: "textos",
                                c: [
                                {
                                    id: 't3',
                                    type: 'text',
                                    rect: ['21px', '24px', '562px', '275px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                },
                                {
                                    id: 'cerrar3',
                                    type: 'image',
                                    rect: ['266px', '330px', '96px', '30px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"cerrar.png",'0px','0px']
                                }]
                            },
                            {
                                id: 'Recurso_15Copy2',
                                type: 'image',
                                rect: ['24px', '181px', '185px', '447px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_15.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage3',
                            type: 'group',
                            rect: ['28', '8', '883', '628', 'auto', 'auto'],
                            c: [
                            {
                                id: 'inst',
                                type: 'group',
                                rect: ['232', '42', '650', '125', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_12',
                                    type: 'image',
                                    rect: ['0px', '0px', '650px', '125px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_12.png",'0px','0px']
                                },
                                {
                                    id: 'Text2',
                                    type: 'text',
                                    rect: ['23px', '57px', '591px', '46px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 20px;\">Lea con atención el caso respecto al cual usted aplicará el modelo de gestión organizacional basado en resultados.</span></p><p style=\"margin: 0px;\">​</p>",
                                    align: "left",
                                    font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                }]
                            },
                            {
                                id: 'box',
                                type: 'group',
                                rect: ['232', '183', '650', '393', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_7Copy',
                                    type: 'image',
                                    rect: ['0px', '0px', '650px', '393px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_7.png",'0px','0px']
                                },
                                {
                                    id: 'Recurso_13',
                                    type: 'image',
                                    rect: ['23px', '26px', '603px', '62px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_13.png",'0px','0px']
                                },
                                {
                                    id: 'btn_caso',
                                    type: 'image',
                                    rect: ['223px', '133px', '204px', '204px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_14.png",'0px','0px']
                                },
                                {
                                    id: 'btn_sig_2',
                                    type: 'image',
                                    rect: ['487px', '326px', '138px', '44px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_10.png",'0px','0px']
                                }]
                            },
                            {
                                id: 'Recurso_15',
                                type: 'image',
                                rect: ['24px', '181px', '185px', '447px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_15.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage4',
                            type: 'group',
                            rect: ['25px', '44', '973', '551', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_16',
                                type: 'image',
                                rect: ['0px', '0px', '328px', '551px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_16.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_18',
                                type: 'image',
                                rect: ['352px', '0px', '621px', '551px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_18.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_20',
                                type: 'image',
                                rect: ['419px', '48px', '96px', '96px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px'],
                                userClass: "efect1"
                            },
                            {
                                id: 'Recurso_21',
                                type: 'image',
                                rect: ['621px', '48px', '96px', '96px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_21.png",'0px','0px'],
                                userClass: "efect1"
                            },
                            {
                                id: 'Recurso_22',
                                type: 'image',
                                rect: ['813px', '48px', '94px', '94px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_22.png",'0px','0px'],
                                userClass: "efect1"
                            },
                            {
                                id: 'Recurso_23',
                                type: 'image',
                                rect: ['368px', '144px', '191px', '379px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_23.png",'0px','0px'],
                                userClass: "efect1"
                            },
                            {
                                id: 'Recurso_24',
                                type: 'image',
                                rect: ['570px', '144px', '191px', '379px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_24.png",'0px','0px'],
                                userClass: "efect1"
                            },
                            {
                                id: 'Recurso_25',
                                type: 'image',
                                rect: ['771px', '144px', '191px', '379px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_25.png",'0px','0px'],
                                userClass: "efect1"
                            },
                            {
                                id: 'Group9',
                                type: 'group',
                                rect: ['10', '13', '952px', '524', 'auto', 'auto'],
                                overflow: 'hidden',
                                c: [
                                {
                                    id: 'g1_1',
                                    type: 'rect',
                                    rect: ['358px', '175px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g1"
                                },
                                {
                                    id: 'g1_2',
                                    type: 'rect',
                                    rect: ['358px', '219px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g1"
                                },
                                {
                                    id: 'g1_3',
                                    type: 'rect',
                                    rect: ['358px', '261px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g1"
                                },
                                {
                                    id: 'g1_4',
                                    type: 'rect',
                                    rect: ['358px', '303px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g1"
                                },
                                {
                                    id: 'g1_5',
                                    type: 'rect',
                                    rect: ['358px', '345px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g1"
                                },
                                {
                                    id: 'g1_6',
                                    type: 'rect',
                                    rect: ['358px', '389px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g1"
                                },
                                {
                                    id: 'g1_7',
                                    type: 'rect',
                                    rect: ['358px', '431px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g1"
                                },
                                {
                                    id: 'g1_8',
                                    type: 'rect',
                                    rect: ['358px', '473px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g1"
                                },
                                {
                                    id: 'g2_1',
                                    type: 'rect',
                                    rect: ['561px', '175px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g2"
                                },
                                {
                                    id: 'g2_2',
                                    type: 'rect',
                                    rect: ['561px', '219px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g2"
                                },
                                {
                                    id: 'g2_3',
                                    type: 'rect',
                                    rect: ['561px', '261px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g2"
                                },
                                {
                                    id: 'g2_4',
                                    type: 'rect',
                                    rect: ['561px', '303px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g2"
                                },
                                {
                                    id: 'g2_5',
                                    type: 'rect',
                                    rect: ['561px', '345px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g2"
                                },
                                {
                                    id: 'g2_6',
                                    type: 'rect',
                                    rect: ['561px', '389px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g2"
                                },
                                {
                                    id: 'g2_7',
                                    type: 'rect',
                                    rect: ['561px', '431px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g2"
                                },
                                {
                                    id: 'g2_8',
                                    type: 'rect',
                                    rect: ['561px', '473px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g2"
                                },
                                {
                                    id: 'g3_1',
                                    type: 'rect',
                                    rect: ['760px', '175px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g3"
                                },
                                {
                                    id: 'g3_2',
                                    type: 'rect',
                                    rect: ['760px', '219px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g3"
                                },
                                {
                                    id: 'g3_3',
                                    type: 'rect',
                                    rect: ['760px', '261px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g3"
                                },
                                {
                                    id: 'g3_4',
                                    type: 'rect',
                                    rect: ['760px', '303px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g3"
                                },
                                {
                                    id: 'g3_5',
                                    type: 'rect',
                                    rect: ['760px', '345px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g3"
                                },
                                {
                                    id: 'g3_6',
                                    type: 'rect',
                                    rect: ['760px', '389px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g3"
                                },
                                {
                                    id: 'g3_7',
                                    type: 'rect',
                                    rect: ['760px', '431px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g3"
                                },
                                {
                                    id: 'g3_8',
                                    type: 'rect',
                                    rect: ['760px', '473px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores g3"
                                },
                                {
                                    id: 'box1',
                                    type: 'rect',
                                    rect: ['0px', '0px', '305px', '524px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                }]
                            },
                            {
                                id: 'btn_info',
                                type: 'image',
                                rect: ['915px', '17px', '39px', '46px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_38',
                                type: 'image',
                                rect: ['460px', '-18px', '407px', '68px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_38.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage5',
                            type: 'group',
                            rect: ['25', '44', '973', '551', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_16Copy',
                                type: 'image',
                                rect: ['0px', '0px', '328px', '551px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_16.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_18Copy',
                                type: 'image',
                                rect: ['352px', '0px', '621px', '551px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_18.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_28',
                                type: 'image',
                                rect: ['519px', '51px', '321px', '122px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_28.png",'0px','0px'],
                                userClass: "efect2"
                            },
                            {
                                id: 'Recurso_29',
                                type: 'image',
                                rect: ['368px', '177px', '191px', '357px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_29.png",'0px','0px'],
                                userClass: "efect2"
                            },
                            {
                                id: 'Recurso_30',
                                type: 'image',
                                rect: ['566px', '177px', '191px', '357px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_30.png",'0px','0px'],
                                userClass: "efect2"
                            },
                            {
                                id: 'Recurso_31',
                                type: 'image',
                                rect: ['765px', '177px', '191px', '357px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_31.png",'0px','0px'],
                                userClass: "efect2"
                            },
                            {
                                id: 'Group2',
                                type: 'group',
                                rect: ['10', '13', '947', '524', 'auto', 'auto'],
                                overflow: 'hidden',
                                c: [
                                {
                                    id: 'g1__1',
                                    type: 'rect',
                                    rect: ['358px', '227px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g1"
                                },
                                {
                                    id: 'g1__2',
                                    type: 'rect',
                                    rect: ['358px', '271px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g1"
                                },
                                {
                                    id: 'g1__3',
                                    type: 'rect',
                                    rect: ['358px', '313px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g1"
                                },
                                {
                                    id: 'g1__4',
                                    type: 'rect',
                                    rect: ['358px', '355px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g1"
                                },
                                {
                                    id: 'g1__5',
                                    type: 'rect',
                                    rect: ['358px', '397px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g1"
                                },
                                {
                                    id: 'g1__6',
                                    type: 'rect',
                                    rect: ['358px', '441px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g1"
                                },
                                {
                                    id: 'g1__7',
                                    type: 'rect',
                                    rect: ['358px', '483px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g1"
                                },
                                {
                                    id: 'g2__1',
                                    type: 'rect',
                                    rect: ['556px', '227px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g2"
                                },
                                {
                                    id: 'g2__2',
                                    type: 'rect',
                                    rect: ['556px', '271px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g2"
                                },
                                {
                                    id: 'g2__3',
                                    type: 'rect',
                                    rect: ['556px', '313px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g2"
                                },
                                {
                                    id: 'g2__4',
                                    type: 'rect',
                                    rect: ['556px', '355px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g2"
                                },
                                {
                                    id: 'g2__5',
                                    type: 'rect',
                                    rect: ['556px', '397px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g2"
                                },
                                {
                                    id: 'g2__6',
                                    type: 'rect',
                                    rect: ['556px', '441px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g2"
                                },
                                {
                                    id: 'g2__7',
                                    type: 'rect',
                                    rect: ['556px', '483px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g2"
                                },
                                {
                                    id: 'g3__1',
                                    type: 'rect',
                                    rect: ['756px', '227px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g3"
                                },
                                {
                                    id: 'g3__2',
                                    type: 'rect',
                                    rect: ['756px', '271px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g3"
                                },
                                {
                                    id: 'g3__3',
                                    type: 'rect',
                                    rect: ['756px', '313px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g3"
                                },
                                {
                                    id: 'g3__4',
                                    type: 'rect',
                                    rect: ['756px', '355px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g3"
                                },
                                {
                                    id: 'g3__5',
                                    type: 'rect',
                                    rect: ['756px', '397px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g3"
                                },
                                {
                                    id: 'g3__6',
                                    type: 'rect',
                                    rect: ['756px', '441px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g3"
                                },
                                {
                                    id: 'g3__7',
                                    type: 'rect',
                                    rect: ['756px', '483px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores2 g3"
                                },
                                {
                                    id: 'box2',
                                    type: 'rect',
                                    rect: ['0px', '0px', '305px', '524px', 'auto', 'auto'],
                                    overflow: 'visible',
                                    fill: ["rgba(255,255,255,1)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                }]
                            },
                            {
                                id: 'btn_info2',
                                type: 'image',
                                rect: ['915px', '17px', '39px', '46px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_39',
                                type: 'image',
                                rect: ['462px', '-17px', '407px', '68px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_39.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage6',
                            type: 'group',
                            rect: ['25', '44', '973', '551', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_16Copy2',
                                type: 'image',
                                rect: ['0px', '0px', '328px', '551px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_16.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_18Copy2',
                                type: 'image',
                                rect: ['352px', '0px', '621px', '551px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_18.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_33',
                                type: 'image',
                                rect: ['566px', '160px', '191px', '357px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_33.png",'0px','0px'],
                                userClass: "efect3"
                            },
                            {
                                id: 'Group4',
                                type: 'group',
                                rect: ['10', '13px', '952px', '524', 'auto', 'auto'],
                                overflow: 'hidden',
                                c: [
                                {
                                    id: 'g3___1',
                                    type: 'rect',
                                    rect: ['556px', '212px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores3 g1"
                                },
                                {
                                    id: 'g3___2',
                                    type: 'rect',
                                    rect: ['556px', '255px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores3 g1"
                                },
                                {
                                    id: 'g3___3',
                                    type: 'rect',
                                    rect: ['556px', '297px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores3 g1"
                                },
                                {
                                    id: 'g3___4',
                                    type: 'rect',
                                    rect: ['556px', '339px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores3 g1"
                                },
                                {
                                    id: 'g3___5',
                                    type: 'rect',
                                    rect: ['556px', '381px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores3 g1"
                                },
                                {
                                    id: 'g3___6',
                                    type: 'rect',
                                    rect: ['556px', '425px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores3 g1"
                                },
                                {
                                    id: 'g3___7',
                                    type: 'rect',
                                    rect: ['556px', '467px', '191px', '38px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.03)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "contenedores3 g1"
                                },
                                {
                                    id: 'box3',
                                    type: 'rect',
                                    rect: ['0px', '0px', '305px', '524px', 'auto', 'auto'],
                                    overflow: 'visible',
                                    fill: ["rgba(255,255,255,1)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                }]
                            },
                            {
                                id: 'btn_info3',
                                type: 'image',
                                rect: ['915px', '17px', '39px', '46px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_32',
                                type: 'image',
                                rect: ['518px', '34px', '333px', '122px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_322.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_40',
                                type: 'image',
                                rect: ['464px', '-17px', '407px', '68px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_40.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'retroalimentacion',
                            type: 'group',
                            rect: ['45', '120', '837', '516', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Group3',
                                type: 'group',
                                rect: ['187', '0', '650', '393', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_7Copy2',
                                    type: 'image',
                                    rect: ['0px', '0px', '650px', '393px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_7.png",'0px','0px']
                                },
                                {
                                    id: 'Recurso_35',
                                    type: 'image',
                                    rect: ['51px', '107px', '549px', '3px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_35.png",'0px','0px']
                                },
                                {
                                    id: 'Text4',
                                    type: 'text',
                                    rect: ['45px', '151px', '564px', '114px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    align: "justify",
                                    font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 'Recurso_343',
                                    type: 'image',
                                    rect: ['44px', '19px', '582px', '91px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_343.png",'0px','0px']
                                }]
                            },
                            {
                                id: 'Recurso_15Copy',
                                type: 'image',
                                rect: ['0px', '69px', '185px', '447px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_15.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'creditos',
                            type: 'group',
                            rect: ['0', '0', '1025', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle',
                                type: 'rect',
                                rect: ['0px', '0px', '1025px', '640px', 'auto', 'auto'],
                                fill: ["rgba(192,192,192,0.48)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'Recurso_362',
                                type: 'image',
                                rect: ['113px', '44px', '806px', '550px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_362.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'btn_credito',
                            type: 'image',
                            rect: ['516px', '599px', '63px', '19px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"Recurso_6.png",'0px','0px']
                        },
                        {
                            id: 'instruccion_final',
                            type: 'group',
                            rect: ['0', '0', '1025', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle5',
                                type: 'rect',
                                rect: ['0px', '0px', '1025px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,0.50)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'Group7',
                                type: 'group',
                                rect: ['177px', '34', '707', '561', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Rectangle6',
                                    type: 'rect',
                                    rect: ['0px', '0px', '707px', '561px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'legend',
                                    type: 'image',
                                    rect: ['68px', '31px', '549px', '84px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"legend.png",'0px','0px']
                                },
                                {
                                    id: 'tf',
                                    type: 'text',
                                    rect: ['62px', '151px', '615px', '173px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​A continuación encontrará las dimensiones del ser humano establecidas en el modelo de gestión organizacional basado en el logro de objetivos con el fin de que aplique dicho modelo según el caso presentado. Como refuerzo de la actividad realizada en el REA 1 del curso, identifique dentro de las opciones que aparecen a la izquierda los objetivos asociados a cada dimensión y arrástrelos a la columna correspondiente.&nbsp;</p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\">2. Puntos a obtener:</p><p style=\"margin: 0px;\">De 0 a 8 aciertos: 1 punto</p><p style=\"margin: 0px;\">De 9 a 16 aciertos: 2 puntos</p><p style=\"margin: 0px;\">De 17 a 24 ciertos: 3 puntos&nbsp;</p><p style=\"margin: 0px;\">​</p>",
                                    align: "justify",
                                    font: ['Arial, Helvetica, sans-serif', [20, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                }]
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle3',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '272px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index4_edgeActions.js");
})("EDGE-20044831");
